package edu.towson.cosc431.shaw.todos

interface ITodoController{
    fun todoCount(): Int;
   fun getTodo(position: Int): Todo
    fun addTodo(todo: Todo)
    fun openTodo(position: Int);
    fun editTodo(position: Int, edits: Todo);
    fun toggleCompleted(position: Int)
    fun removeTodo(position: Int);

    fun showAll();
    fun showActive();
    fun showCompleted();
}