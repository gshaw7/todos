package edu.towson.cosc431.shaw.todos

import java.util.*

 class Todo(val title: String = "", val contents: String = "",var isCompleted: Boolean = false, val image: Any? = null){
     val dateCreated get() = Date();

     override fun toString(): String {
         val completed = if (this.isCompleted) "Yes" else "No";
         return "Title: $title\nTask:$contents\nCompleted: $completed\nCreated: $dateCreated";
     }
}