package edu.towson.cosc431.shaw.todos

class TodoController(val mainActivity: MainActivity) : ITodoController {




    private val todoList: MutableList<Todo> = mutableListOf();
    private val filteredTodos: MutableList<Todo> = mutableListOf();

    private var selectedFilter: TodoFilters.Filter = TodoFilters.Filter.ALL;

    init {
        val todos = (0..9).map{Todo("TODO$it", "Something TODO x $it",false)}
        todoList.addAll(todos);
        filteredTodos.addAll(todoList.map { it });
    }

    override fun addTodo(todo: Todo) {
        todoList.add(todo);
    }

    override fun todoCount(): Int {
        return filteredTodos.size;
    }
    override fun getTodo(position: Int): Todo {
      return filteredTodos[position];
    }

    override fun openTodo(position: Int) {
        val filteredTodo =  filteredTodos[position];
        val actualTodoIndex = todoList.indexOf(filteredTodo);
        val todo = todoList[actualTodoIndex];
        mainActivity.navToEditTodo(actualTodoIndex, todo);
    }

    override fun editTodo(position: Int, edits: Todo) {
       todoList[position] = edits;
        this.updateFilteredList();
    }

    override fun toggleCompleted(position: Int) {
        val filteredTodo =  filteredTodos[position];
        val actualTodoIndex = todoList.indexOf(filteredTodo);
        todoList[actualTodoIndex].isCompleted = !todoList[actualTodoIndex].isCompleted;
        this.updateFilteredList();
    }

    override fun removeTodo(position: Int) {
        val filteredTodo =  filteredTodos[position];
        val actualTodoIndex = todoList.indexOf(filteredTodo);
        todoList.removeAt(actualTodoIndex);
        this.updateFilteredList();
    }

    override fun showAll() {
        this.selectedFilter = TodoFilters.Filter.ALL
        filteredTodos.clear();
        filteredTodos.addAll(todoList.map { it });
    }

    override fun showActive() {
        this.selectedFilter = TodoFilters.Filter.ACTIVE
        filteredTodos.clear();
        filteredTodos.addAll(todoList.filter { todo -> !todo.isCompleted  })
    }

    override fun showCompleted() {
        this.selectedFilter = TodoFilters.Filter.COMPLETED;
        filteredTodos.clear();
        filteredTodos.addAll(todoList.filter { todo -> todo.isCompleted  })
    }

    private fun updateFilteredList(){
        when(this.selectedFilter){
            TodoFilters.Filter.ALL->{
                this.showAll()
            }
            TodoFilters.Filter.ACTIVE->{
                this.showActive()
            }
            TodoFilters.Filter.COMPLETED->{
                this.showCompleted()
            }
        }
    }

}