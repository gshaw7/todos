package edu.towson.cosc431.shaw.todos

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_new_todo.*

class NewTodoActivity : AppCompatActivity() {

    private var editMode: Boolean = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        val intent = intent;
        val existingTodo = intent.getStringExtra(MainActivity.EDIT_TODO_KEY);
        if(existingTodo.isNotEmpty()){
            val todoItem = Gson().fromJson<Todo>(existingTodo, Todo::class.java);
            this.populateTodoData(todoItem);
            save_btn.setOnClickListener { handleEdit() }
        }else{
            save_btn.setOnClickListener { handleSave() }
        }

    }

    private fun populateTodoData(todo: Todo){
        title_input.setText(todo.title)
        text_input.setText(todo.contents)
        is_completed_checkbox.isChecked = todo.isCompleted;
    }

    private fun handleEdit(){
        val newTodo = this.buildTodo();
        val resultIntent = Intent();
        resultIntent.putExtra(RESULT_EDIT_TODO, Gson().toJson(newTodo));
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private fun handleSave(){
        val newTodo = this.buildTodo();
        val resultIntent = Intent();
        resultIntent.putExtra(RESULT_TODO, Gson().toJson(newTodo));
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private fun buildTodo(): Todo{
        val title = title_input.text.toString();
        val contents = text_input.text.toString();
        val isCompleted = is_completed_checkbox.isChecked;
        return Todo(title, contents, isCompleted);
    }

    companion object {
        const val RESULT_TODO = "new_todo";
        const val RESULT_EDIT_TODO = "TODO_EDITED";
        fun newIntent(context: Context): Intent {
           return Intent(context, NewTodoActivity::class.java)
        }
    }
}

