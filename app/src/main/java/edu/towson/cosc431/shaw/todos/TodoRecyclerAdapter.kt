package edu.towson.cosc431.shaw.todos

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.todo_list_item.view.*

class TodoRecyclerAdapter(val controller: ITodoController) :
    RecyclerView.Adapter<TodoRecyclerAdapter.TodoViewHolder>() {

    class TodoViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bind(todoItem: Todo){
            view.todo_title.text = todoItem.title;
            view.todo_content.text = todoItem.contents;
            view.todo_created_date.text = todoItem.dateCreated.toString();
            view.todo_completed_checkbox.isChecked = todoItem.isCompleted;
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TodoViewHolder {
        val todoItemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.todo_list_item, parent, false)
        val holder = TodoViewHolder(todoItemView);
        todoItemView.todo_completed_checkbox.setOnClickListener {
            controller.toggleCompleted(holder.adapterPosition)
            notifyItemChanged(holder.adapterPosition)
        };
        todoItemView.todo_card.setOnClickListener { controller.openTodo(holder.adapterPosition) }
        todoItemView.todo_card.setOnLongClickListener {
            controller.removeTodo(holder.adapterPosition);
            notifyItemRemoved(holder.adapterPosition)
            return@setOnLongClickListener true
        }
        return holder;
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todoItem = controller.getTodo(position);
        holder.bind(todoItem);

    }

    override fun getItemCount() = controller.todoCount()

}