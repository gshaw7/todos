package edu.towson.cosc431.shaw.todos

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), IOnTodoFiltersChangedListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var todoController: ITodoController;

    private var currentTodoEditPos = 0;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        add_todo_btn.setOnClickListener{navToAddTodo()};

        todoController = TodoController(this);

        recyclerView = todo_recycler_view;
        viewAdapter = TodoRecyclerAdapter(todoController);
        viewManager = LinearLayoutManager(this);

        recyclerView.adapter = viewAdapter;
        recyclerView.layoutManager = viewManager;

        val todoFilterWidget = todo_filters_frag as ITodoFilters;
        todoFilterWidget.setOnFilterChangedListener(this);

    }

    override fun onFilterChanged(filter: TodoFilters.Filter) {
       when(filter){
           TodoFilters.Filter.ALL->{
               todoController.showAll();
               viewAdapter.notifyDataSetChanged()

           }
           TodoFilters.Filter.ACTIVE ->{
               todoController.showActive();
               viewAdapter.notifyDataSetChanged()
           }
           TodoFilters.Filter.COMPLETED ->{
               todoController.showCompleted();
               viewAdapter.notifyDataSetChanged()
           }
       }
    }


    private fun navToAddTodo(){
        val intent = NewTodoActivity.newIntent(this);
        intent.putExtra(EDIT_TODO_KEY, "");
        startActivityForResult(intent, TODO_REQUEST_CODE)
    }

     fun navToEditTodo(index: Int, todo: Todo){
         this.currentTodoEditPos = index;
         val todoToString = Gson().toJson(todo, Todo::class.java)
         val intent = NewTodoActivity.newIntent(this);
         intent.putExtra(EDIT_TODO_KEY, todoToString);
         startActivityForResult(intent, EDIT_TODO_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        when(requestCode){
            TODO_REQUEST_CODE ->{
                when(resultCode){
                    Activity.RESULT_OK->{
                        handleTodoSave(data)
                    }
                }
            }
            EDIT_TODO_REQUEST_CODE ->{
                when(resultCode){
                    Activity.RESULT_OK->{
                        handleTodoEdit(data)
                    }
                }
            }
        }
    }

    private fun handleTodoSave(data: Intent?){
        val newTodoString = data?.extras?.getString(NewTodoActivity.RESULT_TODO);
        val newTodo = Gson().fromJson<Todo>(newTodoString, Todo::class.java);
        todoController.addTodo(newTodo);
    }

    private fun handleTodoEdit(data: Intent?){
        val edittedTodoString = data?.extras?.getString(NewTodoActivity.RESULT_EDIT_TODO);
        val edittedTodo = Gson().fromJson<Todo>(edittedTodoString, Todo::class.java);
        todoController.editTodo(this.currentTodoEditPos, edittedTodo);
        viewAdapter.notifyItemChanged(this.currentTodoEditPos);
    }

    companion object {
        const val TODO_REQUEST_CODE = 1;
        const val EDIT_TODO_REQUEST_CODE = 2;
        const val EDIT_TODO_KEY = "EDITTABLE_GPU";
    }
}
