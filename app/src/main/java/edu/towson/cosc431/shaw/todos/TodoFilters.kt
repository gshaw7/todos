package edu.towson.cosc431.shaw.todos


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_todo_filters.*
import kotlinx.android.synthetic.main.fragment_todo_filters.view.*


/**
 * A simple [Fragment] subclass.
 *
 */
class TodoFilters : Fragment(), ITodoFilters {

    private val activeFilterColor = Color.GREEN;
    private var defaultFilterColor: Int = Color.GRAY;

    private lateinit var selectedFilter: Filter;
    private var listener: IOnTodoFiltersChangedListener? = null

    init {
        this.selectedFilter = TodoFilters.Filter.ALL;
    }

    override fun setFilter(filter: Filter) {
        this.selectedFilter = filter;
        this.listener?.onFilterChanged(filter);
    }

    override fun getFilter(): Filter {
        return this.selectedFilter;
    }

    override fun setOnFilterChangedListener(listener: IOnTodoFiltersChangedListener) {
        this.listener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.fragment_todo_filters, container, false)
        view.all_filter_btn.setOnClickListener {  handleClick(it) }
        view.active_filter_btn.setOnClickListener { handleClick(it) }
        view.completed_btn.setOnClickListener { handleClick(it) }
        return view;
    }

    private fun handleClick(btn: View?){
        val filter = when(btn) {
            all_filter_btn -> {
                btn?.setBackgroundColor(this.activeFilterColor)
                active_filter_btn.setBackgroundColor(this.defaultFilterColor);
                completed_btn.setBackgroundColor(this.defaultFilterColor)
                Filter.ALL
            }
            active_filter_btn -> {
                btn?.setBackgroundColor(this.activeFilterColor)
                all_filter_btn.setBackgroundColor(this.defaultFilterColor);
                completed_btn.setBackgroundColor(this.defaultFilterColor)
                Filter.ACTIVE            }
            completed_btn -> {
                btn?.setBackgroundColor(this.activeFilterColor)
                all_filter_btn.setBackgroundColor(this.defaultFilterColor);
                active_filter_btn.setBackgroundColor(this.defaultFilterColor)

                Filter.COMPLETED            }
            else -> {
                btn?.setBackgroundColor(this.activeFilterColor)
                active_filter_btn.setBackgroundColor(this.defaultFilterColor);
                completed_btn.setBackgroundColor(this.defaultFilterColor)
                Filter.ALL}
        }
        listener?.onFilterChanged(filter)
    }

    enum class Filter {ALL, ACTIVE, COMPLETED}

}

interface ITodoFilters{
    fun setFilter(filter: TodoFilters.Filter)
    fun getFilter(): TodoFilters.Filter
    fun setOnFilterChangedListener(listener: IOnTodoFiltersChangedListener)
}
interface IOnTodoFiltersChangedListener {
    fun onFilterChanged(rating: TodoFilters.Filter)
}